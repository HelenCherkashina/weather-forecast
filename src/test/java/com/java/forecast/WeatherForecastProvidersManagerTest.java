package com.java.forecast;

import com.weather.forecast.models.Location;
import com.weather.forecast.models.WeatherData;
import com.weather.forecast.models.WeatherRequest;
import com.weather.forecast.providers.WeatherForecastProvidersManager;
import com.weather.forecast.views.BasicWeatherDataView;
import com.weather.forecast.views.CelsiusTemperatureView;
import com.weather.forecast.views.FahrenheitTemperatureView;
import com.weather.forecast.views.TemperatureView;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.weather.forecast.models.Location.createLocation;
import static com.weather.forecast.models.Scale.CELSIUS;
import static com.weather.forecast.models.Scale.FAHRENHEIT;
import static com.weather.forecast.models.WeatherProvider.DARK_SKY;
import static com.weather.forecast.models.WeatherProvider.GOOGLE;
import static java.math.RoundingMode.HALF_UP;
import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class WeatherForecastProvidersManagerTest {
    private WeatherForecastProvidersManager weatherForecastProvidersManager;

    @Before
    public void setUp() {
        this.weatherForecastProvidersManager = new WeatherForecastProvidersManager();
    }

    @Test
    public void shouldReturnTemperature() {
        //given
        List<Location> locations = Arrays.asList(createLocation(20, 30), createLocation(10, 20));
        List<WeatherData> weatherData = weatherForecastProvidersManager
                .findProvider(GOOGLE)
                .getWeatherData(new WeatherRequest(LocalDate.now(), locations));

        //when
        List<BasicWeatherDataView> basicWeatherDataViews = weatherData.stream().map(BasicWeatherDataView::new).collect(Collectors.toList());

        //then
        assertEquals(2, basicWeatherDataViews.size());
        double temperature1Celsius = basicWeatherDataViews.get(0).getTemperature(CELSIUS).value();
        assertEquals(weatherData.get(0).getTemperature(), temperature1Celsius, 0.0001);
        double temperature1Fahrenheit = basicWeatherDataViews.get(0).getTemperature(FAHRENHEIT).value();
        assertEquals(BigDecimal.valueOf((weatherData.get(0).getTemperature() * 1.8) + 32).setScale(1, HALF_UP).doubleValue(),
                temperature1Fahrenheit, 0.0001);
        double temperature2Celsius = basicWeatherDataViews.get(1).getTemperature(CELSIUS).value();
        assertEquals(weatherData.get(1).getTemperature(), temperature2Celsius, 0.0001);
        double temperature2Fahrenheit = basicWeatherDataViews.get(1).getTemperature(FAHRENHEIT).value();
        assertEquals(BigDecimal.valueOf((weatherData.get(1).getTemperature() * 1.8) + 32).setScale(1, HALF_UP).doubleValue(),
                temperature2Fahrenheit, 0.0001);
    }

    @Test
    public void shouldConvertCelsiusToFahrenheit() {
        //given
        TemperatureView temperature = CelsiusTemperatureView.valueOf(25.0);

        //when
        TemperatureView celsiusTemperatureView = temperature.to(CELSIUS);
        TemperatureView fahrenheitTemperatureView = temperature.to(FAHRENHEIT);

        //then
        assertNotEquals(temperature, celsiusTemperatureView);
        assertNotEquals(temperature, fahrenheitTemperatureView);
        assertNotEquals(celsiusTemperatureView, fahrenheitTemperatureView);
        assertEquals(77.0, fahrenheitTemperatureView.value(), 0.00001);
        assertEquals(25.0, celsiusTemperatureView.value(), 0.00001);
    }

    @Test
    public void shouldConvertFahrenheitToCelsius() {
        //given
        TemperatureView temperature = FahrenheitTemperatureView.valueOf(852.8);

        //when
        TemperatureView celsiusTemperatureView = temperature.to(CELSIUS);
        TemperatureView fahrenheitTemperatureView = temperature.to(FAHRENHEIT);

        //then
        assertNotEquals(temperature, celsiusTemperatureView);
        assertNotEquals(temperature, fahrenheitTemperatureView);
        assertNotEquals(celsiusTemperatureView, fahrenheitTemperatureView);
        assertEquals(852.8, fahrenheitTemperatureView.value(), 0.00001);
        assertEquals(456, celsiusTemperatureView.value(), 0.00001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionIfNoImplementationForProviderExists() {
        //given

        //when
        weatherForecastProvidersManager.findProvider(DARK_SKY);

        //then
    }
}
