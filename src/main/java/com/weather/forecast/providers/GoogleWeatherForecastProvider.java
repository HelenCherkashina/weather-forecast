package com.weather.forecast.providers;

import com.weather.forecast.models.WeatherData;
import com.weather.forecast.models.WeatherRequest;
import com.weather.forecast.models.WindData;

import java.time.LocalTime;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.weather.forecast.models.Condition.randomCondition;
import static com.weather.forecast.models.WeatherData.createMockWeatherData;

/**
 * Google service to fetch weather data
 */
public class GoogleWeatherForecastProvider implements WeatherForecastProvider {
    /**
     * Returns weather data for the given request
     *
     * @param weatherRequest requested weather input locations and date
     * @return list of weather data by locations
     */
    @Override
    public List<WeatherData> getWeatherData(WeatherRequest weatherRequest) {
        Random random = new Random();
        double temperature = 100 * random.nextDouble();
        double humidity = 100 * random.nextDouble();
        return weatherRequest.getLocations().stream()
                .map(location -> createMockWeatherData(weatherRequest.getDate(), location, randomCondition(), temperature, humidity, createWindData()))
                .collect(Collectors.toList());
    }

    private List<WindData> createWindData() {
        LocalTime time = LocalTime.of(0, 0);
        return IntStream.range(0, 12)
                .mapToObj(hour -> WindData.createWindData(time.plusHours(hour), new Random().nextDouble()))
                .collect(Collectors.toList());
    }
}