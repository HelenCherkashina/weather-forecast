package com.weather.forecast.providers;

/**
 * Service providers manager
 *
 * @param <T> type of the service provider
 * @param <P> type of the returned provider
 */
public interface ProvidersManager<T, P> {
    /**
     * Find a provider for the given type
     *
     * @param provider type of the searchable provider
     * @return provider service
     */
    P findProvider(T provider);
}