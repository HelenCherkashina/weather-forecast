package com.weather.forecast.providers;

import com.weather.forecast.models.WeatherProvider;

import java.util.HashMap;
import java.util.Map;

import static com.weather.forecast.models.WeatherProvider.GOOGLE;

/**
 * Weather forecast providers manager
 */
public class WeatherForecastProvidersManager implements ProvidersManager<WeatherProvider, WeatherForecastProvider> {
    private static Map<WeatherProvider, WeatherForecastProvider> providers = new HashMap<>();

    static {
        providers.put(GOOGLE, new GoogleWeatherForecastProvider());
    }

    /**
     * Find weather forecast provider for the given type
     *
     * @param provider type of the searchable provider
     * @return weather forecast provider
     *
     * @throws IllegalArgumentException if no implementation for
     * the given provider type is found
     */
    @Override
    public WeatherForecastProvider findProvider(WeatherProvider provider) {
        if (!providers.containsKey(provider)) {
            throw new IllegalArgumentException("Can't find registered provider " + provider.name());
        }

        return providers.get(provider);
    }
}
