package com.weather.forecast.providers;

import com.weather.forecast.models.WeatherData;
import com.weather.forecast.models.WeatherRequest;

import java.util.List;

/**
 * Service to fetch weather data
 */
public interface WeatherForecastProvider {
    /**
     * Returns weather data for the given request
     *
     * @param weatherRequest requested weather input data
     * @return list of weather data by locations
     */
    List<WeatherData> getWeatherData(WeatherRequest weatherRequest);
}