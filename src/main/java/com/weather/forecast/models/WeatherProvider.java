package com.weather.forecast.models;

/**
 * Types of weather providers
 */
public enum WeatherProvider {
    GOOGLE("http://google.whatever.com/api/v1"),
    DARK_SKY("http://dark.sky.whatever.com/api/v1");

    private final String url;

    WeatherProvider(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
