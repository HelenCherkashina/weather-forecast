package com.weather.forecast.models;

import java.time.LocalTime;

/**
 * Wind data
 */
public class WindData {
    private final LocalTime time;
    private final double value;

    private WindData(LocalTime time, double value) {
        this.time = time;
        this.value = value;
    }

    /**
     * @return information about time
     */
    public LocalTime getTime() {
        return time;
    }

    /**
     * @return information speed for time
     */
    public double getValue() {
        return value;
    }

    public static WindData createWindData(LocalTime time, double value) {
        return new WindData(time, value);
    }
}