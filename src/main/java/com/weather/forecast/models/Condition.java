package com.weather.forecast.models;

import java.util.Random;

/**
 * Weather condition
 */
public enum  Condition {
    SUNNY, CLOUDY, RAINY, WINDY;

    public static Condition randomCondition() {
        return values()[new Random().nextInt(values().length - 1)];
    }
}