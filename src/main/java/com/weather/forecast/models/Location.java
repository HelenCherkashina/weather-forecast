package com.weather.forecast.models;

/**
 * Represents geographical point
 */
public class Location {
    private final double latitude;
    private final double longitude;

    private Location(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public static Location createLocation(double latitude, double longitude) {
        return new Location(latitude, longitude);
    }
}