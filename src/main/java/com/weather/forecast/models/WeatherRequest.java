package com.weather.forecast.models;

import java.time.LocalDate;
import java.util.List;

import static java.util.Collections.unmodifiableList;

public class WeatherRequest {
    private final LocalDate date;
    private final List<Location> locations;
    private final Scale scale = Scale.CELSIUS;

    public WeatherRequest(LocalDate date, List<Location> locations) {
        this.date = date;
        this.locations = locations;
    }

    public LocalDate getDate() {
        return date;
    }

    public List<Location> getLocations() {
        return unmodifiableList(locations);
    }

    public Scale getScale() {
        return scale;
    }
}