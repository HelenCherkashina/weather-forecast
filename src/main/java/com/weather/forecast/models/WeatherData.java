package com.weather.forecast.models;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.unmodifiableList;

/**
 * Weather data
 */
public class WeatherData {
    private final LocalDate date;
    private final Location location;
    private final Condition condition;
    private final double temperature;
    private final double humidity;
    private final List<WindData> windData;

    private WeatherData(LocalDate date, Location location, Condition condition, double temperature, double humidity, List<WindData> windData) {
        this.date = date;
        this.location = location;
        this.condition = condition;
        this.temperature = temperature;
        this.humidity = humidity;
        this.windData = windData;
    }

    /**
     * @return date for the given forecast
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * @return location for the given forecast
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @return condition for the given forecast
     */
    public Condition getCondition() {
        return condition;
    }

    /**
     * @return temperature for the given forecast
     */
    public double getTemperature() {
        return temperature;
    }

    /**
     * @return humidity for the given forecast
     */
    public double getHumidity() {
        return humidity;
    }

    /**
     * @return list of wind data
     */
    public List<WindData> getWindData() {
        return unmodifiableList(windData);
    }

    public static WeatherData createMockWeatherData(LocalDate date, Location location, Condition condition, double temperature, double humidity, List<WindData> windData) {
        return new WeatherData(date, location, condition, temperature, humidity, windData);
    }
}
