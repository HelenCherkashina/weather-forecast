package com.weather.forecast.models;

/**
 * TemperatureView scale
 */
public enum Scale {
    CELSIUS, FAHRENHEIT
}
