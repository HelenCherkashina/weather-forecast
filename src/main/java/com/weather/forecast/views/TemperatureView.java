package com.weather.forecast.views;

import com.weather.forecast.models.Scale;

/**
 * Basic temperature view
 */
public interface TemperatureView {
    /**
     * @return temperature double value
     */
    double value();

    /**
     * @return temperature value based on scale
     */
    TemperatureView to(Scale scale);
}