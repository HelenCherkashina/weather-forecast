package com.weather.forecast.views;

import com.weather.forecast.models.Condition;
import com.weather.forecast.models.Location;
import com.weather.forecast.models.Scale;

import java.util.List;

/**
 * Basic weather data view
 */
public interface WeatherDataView {
    /**
     * @param format format of the date
     * @return date in the requested format
     */
    String getDate(String format);

    /**
     * @return location for weather forecast
     */
    Location getLocation();

    /**
     * @return condition for weather forecast
     */
    Condition getCondition();

    /**
     * @return temperature for weather forecast
     */
    TemperatureView getTemperature(Scale scale);

    /**
     * @return humidity for weather forecast
     */
    double getHumidity();

    /**
     * @return information of wind changes
     */
    List<WindDataView> getWindData();
}