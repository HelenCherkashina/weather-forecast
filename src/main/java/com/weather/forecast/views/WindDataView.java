package com.weather.forecast.views;

import com.weather.forecast.models.WindData;

import static java.time.format.DateTimeFormatter.ofPattern;

/**
 * View for the wind data changes
 */
public class WindDataView {
    private WindData windData;

    WindDataView(WindData windData) {
        this.windData = windData;
    }

    /**
     * @return information about time
     */
    public String getTime() {
        return windData.getTime().format(ofPattern("hh:mm"));
    }

    /**
     * @return information speed for time
     */
    public double getValue() {
        return windData.getValue();
    }
}