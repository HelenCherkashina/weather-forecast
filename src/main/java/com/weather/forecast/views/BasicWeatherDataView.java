package com.weather.forecast.views;

import com.weather.forecast.models.Condition;
import com.weather.forecast.models.Location;
import com.weather.forecast.models.Scale;
import com.weather.forecast.models.WeatherData;

import java.util.List;
import java.util.stream.Collectors;

import static com.weather.forecast.views.CelsiusTemperatureView.valueOf;
import static java.time.format.DateTimeFormatter.ofPattern;

/**
 * Basic weather data view
 */
public class BasicWeatherDataView implements WeatherDataView {
    private WeatherData weatherData;

    public BasicWeatherDataView(WeatherData weatherData) {
        this.weatherData = weatherData;
    }

    /**
     * @param format format of the date
     * @return date in the requested format
     */
    @Override
    public String getDate(String format) {
        return weatherData.getDate().format(ofPattern(format));
    }

    /**
     * @return location for weather forecast
     */
    @Override
    public Location getLocation() {
        return weatherData.getLocation();
    }

    /**
     * @return condition for weather forecast
     */
    @Override
    public Condition getCondition() {
        return weatherData.getCondition();
    }

    /**
     * Temperature for weather forecast for the given scale
     *
     * @param scale
     * @return
     */
    @Override
    public TemperatureView getTemperature(Scale scale) {
        return valueOf(weatherData.getTemperature()).to(scale);
    }

    /**
     * @return humidity for weather forecast
     */
    @Override
    public double getHumidity() {
        return weatherData.getHumidity();
    }

    /**
     * @return information of wind changes
     */
    @Override
    public List<WindDataView> getWindData() {
        return weatherData.getWindData().stream().map(WindDataView::new).collect(Collectors.toList());
    }
}
