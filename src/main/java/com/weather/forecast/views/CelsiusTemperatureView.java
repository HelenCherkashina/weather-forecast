package com.weather.forecast.views;

import com.weather.forecast.models.Scale;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static com.weather.forecast.models.Scale.CELSIUS;
import static com.weather.forecast.models.Scale.FAHRENHEIT;

/**
 * Representation of temperature of celsius scale
 */
public class CelsiusTemperatureView implements TemperatureView {
    private static Map<Scale, Function<Double, TemperatureView>> CONVERTERS = new HashMap<>();

    private final double temperature;

    static {
        CONVERTERS.put(CELSIUS, CelsiusTemperatureView::valueOf);
        CONVERTERS.put(FAHRENHEIT, CelsiusTemperatureView::toFahrenheit);
    }

    private CelsiusTemperatureView(double temperature) {
        this.temperature = temperature;
    }

    /**
     * Create object of celsius temperature view
     *
     * @param temperatureInCelsius temperature value
     * @return celsius temperature representation
     */
    public static TemperatureView valueOf(double temperatureInCelsius) {
        return new CelsiusTemperatureView(temperatureInCelsius);
    }

    /**
     * @return temperature double value
     */
    @Override
    public double value() {
        return temperature;
    }

    /**
     * Convert temperature to another scale
     *
     * @param scale scale to convert temperature value
     * @return new object with converted temperature value
     */
    @Override
    public TemperatureView to(Scale scale) {
        return CONVERTERS.get(scale).apply(temperature);
    }

    private static TemperatureView toFahrenheit(double temperature) {
        BigDecimal rate = BigDecimal.valueOf(9).divide(BigDecimal.valueOf(5), 10, RoundingMode.HALF_UP);
        BigDecimal result = BigDecimal.valueOf(temperature).multiply(rate).add(BigDecimal.valueOf(32));
        return valueOf(result.setScale(1, RoundingMode.HALF_UP).doubleValue());
    }
}
