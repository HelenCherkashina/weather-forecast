package com.weather.forecast.views;

import com.weather.forecast.models.Scale;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static com.weather.forecast.models.Scale.CELSIUS;
import static com.weather.forecast.models.Scale.FAHRENHEIT;

/**
 * Representation of temperature of fahrenheit scale
 */
public class FahrenheitTemperatureView implements TemperatureView {
    private static Map<Scale, Function<Double, TemperatureView>> CONVERTERS = new HashMap<>();

    private final double temperature;

    static {
        CONVERTERS.put(CELSIUS, FahrenheitTemperatureView::toCelsius);
        CONVERTERS.put(FAHRENHEIT, FahrenheitTemperatureView::valueOf);
    }

    private FahrenheitTemperatureView(double temperature) {
        this.temperature = temperature;
    }

    /**
     * Create object of fahrenheit temperature view
     *
     * @param temperatureInCelsius temperature value
     * @return fahrenheit temperature representation
     */
    public static TemperatureView valueOf(double temperatureInCelsius) {
        return new FahrenheitTemperatureView(temperatureInCelsius);
    }

    /**
     * @return temperature double value
     */
    @Override
    public double value() {
        return temperature;
    }

    /**
     * Convert temperature to another scale
     *
     * @param scale scale to convert temperature value
     * @return new object with converted temperature value
     */
    @Override
    public TemperatureView to(Scale scale) {
        return CONVERTERS.get(scale).apply(temperature);
    }

    private static TemperatureView toCelsius(double temperature) {
        BigDecimal rate = BigDecimal.valueOf(5).divide(BigDecimal.valueOf(9), 10, RoundingMode.HALF_UP);
        BigDecimal result = BigDecimal.valueOf(temperature).subtract(BigDecimal.valueOf(32)).multiply(rate);
        return valueOf(result.setScale(1, RoundingMode.HALF_UP).doubleValue());
    }
}
