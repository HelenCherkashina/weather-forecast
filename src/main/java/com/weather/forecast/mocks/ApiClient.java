package com.weather.forecast.mocks;

import java.util.Map;

@Mock
public class ApiClient {
    public Object get(String url, Map<String, String> params) {
        return null;
    }
    public Object post(String url, Map<String, String> params, Object body) {
        return null;
    }
}